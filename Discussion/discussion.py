# [SECTION] Lists
# lists in Python is similar to the arrays in JS. They both provide a collection of data

names = ["John", "Paul", "George", "Ringo"]
programs = ['developer career', 'pi-shape', 'short courses']
durations = [360, 180, 20]
truth_values = [ True, False, True, True, False]

print(names)

#  list with different data type
sample_list = ["Apple", 3, False, 'Potato', 4, True] # this is allowed in python, just keep in mind the convention of having the same data type in each of the list
print(sample_list)

# getting the size of a list
print(len(programs))
# print(len("Apple")) it can also be used in strings

# accessing the values inside the lists
print(durations[1])
print(programs[0])
# since python has negative indeces, we can commonly use it to check the last element of the list
print(names[-1]) # last element
print(names[-3]) # third element from the end of the list

# accessing non-existent indeces
# print(truth_values[20]) # would return an error: index out of range

# Range of Indeces
print(names[0:2]) # from index 0, print the elements until before index 2

"""
[Section] Mini Activity
Mini activity
1. Create a list of names of 5 students
2. Create a list of grades for the 5 students
3. Using loop, iterate through the list and print the grade of students > is <grade>
"""
student_name = ["Bernard", "Herbert", "Ragem", "Rhica", "Cris"]
student_grade = [89.9, 90.5, 92, 91.1, 90]

j = 0
while j < len(student_name) :
	print(f"The grade of {student_name[j]} is {student_grade[j]}")
	if j == len(student_name) :
		break
	j += 1
print("")

"""
End of Mini Activity
"""

# Updating Lists
# Print the current value
print(f"Current Value: {names[2]}")

# Update the value
names[2] = 'Michael'

# Print the new line
print(f"New Value: {names[2]}")


# List Manipulation
# List have methods that can be used to manipulate the elements within

# Adding List Items - the append() method allows inserting items to the end of the list
names.append("Bobby")
print(names)

# Deleting List Items - the "del" keyword can be used to delete items from a list
durations.append(360)
print(durations)

# Delete the last item from durations
del durations[-1]
print(durations)

# Membership checks - the "in" keyword checks if a given elements is in the list and returns true or false
print(20 in durations)
print(500 in durations)

# Sorting Lists - the sort() method sorts the list alphanumerically in ascending order by default
names.sort()
print(names)

# count = 0
# while count < 5 :
# 	print(f)

count = 0
while count < len(names) :
	print(names[count])
	count += 1
print("")

# Dictionaries
# Dictionaries are used to store data in key:value pairs, similar to Objects in JavaScript. Dictonaries are collections which are ordered, changeable, and do not allow duplicates.

# Order means that items have a defined order that cannot be changed
# Changeable means that values can be changed

person1 = {
	"name": "Brandon",
	"age": "28",
	"occupation": "student",
	"isEnrolled": True,
	"subject": ["Python", "SQL", "Django"]
}

# To get the number of key:value pairs in a dictionary, the len() method can be used again
print(len(person1))
print("")

# Accessing value in Dictionaries
# To get the value of an item in a dictionary, the key name can be used with a pair of square brackets
print(person1["name"])
print("")

# The keys() and values() method will return a list of all keys/values int the dictionary
print(person1.keys())

print(person1.values())
print("")

# check the datat type of a value
print(type(person1))
print(type(names))
print("")

# The items() method will return each item in the dictionary, as key:value pair in a list
print(person1.items())
print("")

# Adding key:value pairs can be done by either putting a new index key and assigning a value or by using the update() method
person1["nationality"] = "Filipino"
person1.update({"fav_food": "BBQ"})
print(person1)
print("")

# Deleting entries can be done using the pop() method or the del keyword
person1.pop("fav_food")
del person1["nationality"]
print(person1)
print("")


# Looping through dictionaries
for key in person1 :
	print(f"The value of {key} is {person1[key]}")
print("")

# Nested Dictionaries
person2 = {
	"name": "Monika",
	"age": "28",
	"occupation": "student",
	"isEnrolled": True,
	"subject": ["Python", "SQL", "Django"]
}

classRoom = {
	"student1": person1,
	"student2": person2
}
print(classRoom)
print("")

# functions
# functions are blocks of code that can be run when called/invoked. A function can be used to get input, process the input, and return output.

# The "def" keyword is used to create a function
def my_greeting() :
	# code to be executed when function is invoked
	print("Hello user!")
print("")

# Calling/invoking the function
my_greeting()

# Parameters can be added to functions to have more control as to what input the function will need
def greet_user(username) :
	# prints out the value of the username parameters
	print(f"Hello, {username}!")
print("")

greet_user("Bob")
greet_user("Amy")
print("")


# Return statement - the "return" keyword allows function to return values, just like in JavaScript

def addition(num1, num2) :
	return num1 + num2
sum = addition(5, 10)
print(f"The sum is {sum}")
print("")

# Lambda Function
# A Lambda Function is a small anonymous function that can be used for callbacks. It is just like a normal Python function, except that its name is defined as a variable, and usually contains just one line code. A Lambda Function can take any number of arguments, but can only have one expression

greeting = lambda person : f"Hello {person}"
print(greeting("Anthony"))

mult = lambda a, b : a * b 
print(mult(5,6))


def increaser(num1) : 
	return lambda num2: num2 * num1

doubler = increaser(2)

print(doubler(11))

tripler = increaser(3)

print(tripler(7))

# Discussion
# https://gitlab.com/tuitt/students/batch220/resources/s03/discussion