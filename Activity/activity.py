# 1. Create a car dictionary with the following keys: brand, model, year of make, color (any values)

# 2. Print the following statement from the details: "I own a (color) (brand) (model) and it was made in (year_of_make)"

# 3. Create a function that gets the square of a number

# 4. Create a function that takes one of the following languages as its parameter:

# a. French
# b. Spanish
# c. Japanese

# Depending on which language is given, the function prints "Hello World!" in that language. Add a condition that asks the user to input a valid language if the given parameter does not match any of the above.
import random

# Answer of number 1 and 2
car = {
	"brand": "Honda",
	"model": "City",
	"year_of_make": 2022,
	"color": "Crystal Black Pearl"
}
print(f"I own a {car['color']} {car['brand']} {car['model']} and it was made in {car['year_of_make']}")
print("")


# Answer of number 3
def square(number) :
	return number * number
number_entry = int(input("Enter a number: "))
result = square(number_entry)
print(f"The square of {number_entry} is {result}")
print("")


# Answer of number 4
def language(result) :
	if result == 'FRENCH' :
		print("Bonjour le monde!")
	elif result == 'SPANISH' :
		print("Hola Mundo!")
	elif result == 'JAPANESE' :
		print("Kon'nichiwa sekai")
	else :
		print("Language can't find")

my_language = input("Enter your language: ")
language(my_language.upper())
print("")


print("Random Language")
def x(result) :
	if result == 'FRENCH' :
		print("French: Bonjour le monde!")
	elif result == 'SPANISH' :
		print("Spanish: Hola Mundo!")
	elif result == 'JAPANESE' :
		print("Japanese: Kon'nichiwa sekai")
	else :
		print("Language can't find")

my_list = ["FRENCH", "SPANISH", "JAPANESE"]
x(random.choice(my_list))
	

